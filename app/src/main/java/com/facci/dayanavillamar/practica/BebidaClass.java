package com.facci.dayanavillamar.practica;

public class BebidaClass {

    private String nombre;
    private String precio;
    private String descripcion;

    public BebidaClass(){

    }

    public BebidaClass(String nombre, String precio, String descripcion){

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
