package com.facci.dayanavillamar.practica;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BebidaAdapter extends FirestoreRecyclerAdapter<BebidaClass, BebidaAdapter.ViewHolder> {

    Activity activity;
    FirebaseFirestore firestore;

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public BebidaAdapter(@NonNull FirestoreRecyclerOptions<BebidaClass> options, Activity activity) {
        super(options);
        this.activity = activity;
        firestore = FirebaseFirestore.getInstance();
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder viewHolder, int i, @NonNull BebidaClass bebidaClass) {

        DocumentSnapshot bebidaDocument = getSnapshots().getSnapshot(viewHolder.getAdapterPosition());
        String id = bebidaDocument.getId();

        viewHolder.txt_view_nombre.setText(bebidaClass.getNombre());
        viewHolder.txt_view_precio.setText(bebidaClass.getPrecio());
        viewHolder.txt_view_descripcion.setText(bebidaClass.getDescripcion());

        viewHolder.btn_edit_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, EditActivity.class);
                intent.putExtra("bebidaId", id);
                activity.startActivity(intent);
            }
        });

        viewHolder.btn_delete_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firestore.collection("Bebidas").document(id).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(activity, "Se eliminaron los datos correctamente", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener(){
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(activity, "No se eliminaron los datos", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_drinks, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView txt_view_nombre;
        TextView txt_view_precio;
        TextView txt_view_descripcion;
        Button btn_edit_drink, btn_delete_drink;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_view_nombre = itemView.findViewById(R.id.id_out_read_name_drink);
            txt_view_precio = itemView.findViewById(R.id.id_out_read_price_drink);
            txt_view_descripcion = itemView.findViewById(R.id.id_out_read_description_drink);
            btn_edit_drink = itemView.findViewById(R.id.id_btn_update_drink);
            btn_delete_drink = itemView.findViewById(R.id.id_btn_delete_drink);
        }
    }

}
