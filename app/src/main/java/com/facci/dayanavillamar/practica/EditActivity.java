package com.facci.dayanavillamar.practica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class EditActivity extends AppCompatActivity {

    private String bebidaId;
    private FirebaseFirestore firestore;

    EditText update_name_drink, update_price_drink, update_description_drink;
    Button btn_update_drink;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        update_name_drink = findViewById(R.id.id_update_name_drink);
        update_price_drink = findViewById(R.id.id_update_price_drink);
        update_description_drink = findViewById(R.id.id_update_description_drink);
        btn_update_drink = findViewById(R.id.id_btn_update_drink);

        firestore = FirebaseFirestore.getInstance();
        bebidaId = getIntent().getStringExtra("bebidaId");

        inData();

        btn_update_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
            }
        });
    }
    private void inData(){
        firestore.collection("Bebidas").document(bebidaId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.exists()){
                    String nombre = documentSnapshot.getString("nombre");
                    String precio = documentSnapshot.getString("precio");
                    String descripcion = documentSnapshot.getString("descripcion");

                    update_name_drink.setText(nombre);
                    update_price_drink.setText(precio);
                    update_description_drink.setText(descripcion);
                }
            }
        });
    }
    private void updateData(){

        String nombre = update_name_drink.getText().toString();
        String precio = update_price_drink.getText().toString();
        String descripcion = update_description_drink.getText().toString();

        Map<String, Object> map = new HashMap<>();
        map.put("nombre", nombre);
        map.put("precio",precio);
        map.put("descripcion", descripcion);
        firestore.collection("Bebidas").document(bebidaId).update(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(EditActivity.this, "Los datos han sido actualizados correctamente", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(EditActivity.this, ReadActivity.class));
                finish();
            }
        });
    }
}