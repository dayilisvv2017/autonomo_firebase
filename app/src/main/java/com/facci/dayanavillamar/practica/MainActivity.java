package com.facci.dayanavillamar.practica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private EditText in_text_user,in_text_email,in_text_pass;
    private Button btn_register_user,btn_go_login;
    private String user = "";
    private String email = "";
    private String pass = "";
    DatabaseReference FireDatabase;
    FirebaseAuth Auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        in_text_user = (EditText) findViewById(R.id.id_in_user);
        in_text_email = (EditText) findViewById(R.id.id_in_email);
        in_text_pass = (EditText) findViewById(R.id.id_in_pass);
        btn_register_user = (Button) findViewById(R.id.id_btn_register_user);
        btn_go_login = (Button) findViewById(R.id.id_btn_go_login_user);

        Auth = FirebaseAuth.getInstance();
        FireDatabase = FirebaseDatabase.getInstance().getReference();

        btn_register_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user = in_text_user.getText().toString();
                email = in_text_email.getText().toString();
                pass = in_text_pass.getText().toString();

                if (!user.isEmpty() && !email.isEmpty() && !pass.isEmpty()){
                    if (pass.length() >= 6){
                        registerUser();
                    }
                    else{
                        Toast.makeText(MainActivity.this, "La contraseña debe tener al menos 6 caracteres", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(MainActivity.this, "Debe completar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_go_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });
    }
    private void registerUser(){
        Auth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){

                    Map<String, Object> map = new HashMap<>();
                    map.put("usuario", user);
                    map.put("correo", email);
                    map.put("contraseña", pass);

                    String id = Auth.getCurrentUser().getUid();



                    FireDatabase.child("Users").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                                finish();
                            }else{
                                Toast.makeText(MainActivity.this, "No se pudieron crear los datos correctamente", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(MainActivity.this, "No se pudo registrar", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}