package com.facci.dayanavillamar.practica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileActivity extends AppCompatActivity {

    private Button btn_logout,btn_add_drink, btn_read_drink;
    private TextView user_actual,email_actual;
    private FirebaseAuth Auth;
    private DatabaseReference Database_Firestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Auth = FirebaseAuth.getInstance();
        Database_Firestore = FirebaseDatabase.getInstance().getReference();
        btn_logout = (Button) findViewById(R.id.id_btn_logout);
        btn_add_drink = (Button) findViewById(R.id.id_btn_add_drink);
        btn_read_drink = (Button) findViewById(R.id.id_btn_read_drink);
        user_actual = (TextView) findViewById(R.id.id_user_actual);
//        email_actual = (TextView) findViewById(R.id.id_email_actual);

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Auth.signOut();
                startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                finish();
            }
        });
        btn_add_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this, DrinksActivity.class));
            }
        });
        btn_read_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this, ReadActivity.class));
            }
        });
        getUserInfo();
    }
    private void getUserInfo(){
        String id = Auth.getCurrentUser().getUid();
        Database_Firestore.child("Users").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String user = snapshot.child("usuario").getValue().toString();
                    //String email = snapshot.child("correo").getValue().toString();
                    user_actual.setText(user);
                    //email_actual.setText(email);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}