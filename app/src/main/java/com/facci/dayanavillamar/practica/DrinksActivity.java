package com.facci.dayanavillamar.practica;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.HashMap;
import java.util.Map;

public class DrinksActivity extends AppCompatActivity {

    EditText edit_name_drink, edit_price_drink, edit_description_drink;
    Button btn_add_drink;
    TextView txt_out_data;

    FirebaseFirestore firestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drinks);

        firestore = FirebaseFirestore.getInstance();

        edit_name_drink = findViewById(R.id.id_in_name_drink);
        edit_price_drink = findViewById(R.id.id_in_price_drink);
        edit_description_drink = findViewById(R.id.id_in_description_drink);
        btn_add_drink = findViewById(R.id.id_btn_register_drink);
        txt_out_data = findViewById(R.id.id_out_data);

        btn_add_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createData();
            }
        });
    }

    private void createData(){

        String nombre = edit_name_drink.getText().toString();
        String precio = edit_price_drink.getText().toString();
        String descripcion = edit_description_drink.getText().toString();

        Map<String, Object> map = new HashMap<>();
        map.put("nombre", nombre);
        map.put("precio", precio);
        map.put("descripcion", descripcion);

        //firestore.collection("Bebidas").document().set(map);
        firestore.collection("Bebidas").add(map).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(DrinksActivity.this, "Los datos se cargaron correctamente", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(DrinksActivity.this, ReadActivity.class));
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(DrinksActivity.this, "No se han cargado los datos", Toast.LENGTH_SHORT).show();
            }
        });

    }
}