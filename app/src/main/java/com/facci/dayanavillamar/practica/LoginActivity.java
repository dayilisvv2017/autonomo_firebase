package com.facci.dayanavillamar.practica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    private EditText in_text_email,in_text_pass;
    private Button btn_login_user, btn_go_register_user;

    private String email = "";
    private String pass = "";

    private FirebaseAuth Auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Auth = FirebaseAuth.getInstance();

        in_text_email = (EditText) findViewById(R.id.id_in_email);
        in_text_pass = (EditText) findViewById(R.id.id_in_pass);
        btn_login_user = (Button) findViewById(R.id.id_btn_login_user);
        btn_go_register_user = (Button) findViewById(R.id.id_btn_go_register_user);

        btn_login_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = in_text_email.getText().toString();
                pass = in_text_pass.getText().toString();

                if (!email.isEmpty() && !pass.isEmpty()){

                    loginUser();

                }else{
                    Toast.makeText(LoginActivity.this, "Debe completar todos los campos", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_go_register_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
        });

    }
    private void loginUser(){
        Auth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    startActivity(new Intent(LoginActivity.this, ProfileActivity.class));
                }else{
                    Toast.makeText(LoginActivity.this, "No se pudo iniciar sesion, compruebe los datos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Auth.getCurrentUser() != null){
            startActivity(new Intent(LoginActivity.this, ProfileActivity.class));
            finish();
        }
    }
}