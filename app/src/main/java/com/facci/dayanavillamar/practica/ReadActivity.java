package com.facci.dayanavillamar.practica;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

public class ReadActivity extends AppCompatActivity {

    TextView txt_out_data;

    FirebaseFirestore firestore;

    RecyclerView recyclerViewBebidas;
    BebidaAdapter bebidaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        firestore = FirebaseFirestore.getInstance();
/*
        txt_out_data = findViewById(R.id.id_out_data);

        readData();
    }*/

//    private void readData(){
//
//        firestore.collection("Bebidas").document("dHLFj9KeFEt5hkYR9Qaj").addSnapshotListener(new EventListener<DocumentSnapshot>() {
//            @Override
//            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
//                if (value.exists()){
//                    String nombre = "Por defecto";
//                    String precio = "Por defecto";
//                    if(value.contains("nombre")){
//                        nombre = value.getString("nombre");
//                    }
//                    if(value.contains("precio")){
//                        precio = value.getString("precio");
//                    }
//                    txt_out_data.setText("Nombre: " + nombre + "\n" + "Precio: " + precio);
//                }
//            }
//        });
        recyclerViewBebidas = findViewById(R.id.id_recicle_bebida);
        recyclerViewBebidas.setLayoutManager(new LinearLayoutManager(this));

        Query query = firestore.collection("Bebidas");

        FirestoreRecyclerOptions<BebidaClass> firestoreRecyclerOptions = new
                FirestoreRecyclerOptions.Builder<BebidaClass>().setQuery(query, BebidaClass.class).build();
        bebidaAdapter = new BebidaAdapter(firestoreRecyclerOptions, this);
        bebidaAdapter.notifyDataSetChanged();
        recyclerViewBebidas.setAdapter(bebidaAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bebidaAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        bebidaAdapter.stopListening();
    }
}